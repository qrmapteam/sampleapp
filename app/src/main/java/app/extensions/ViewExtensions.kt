package app.extensions

import android.widget.ImageView
import app.R
import app.ext.GlideApp
import com.bumptech.glide.load.resource.bitmap.FitCenter

fun ImageView.loadImage(url: String?) {

    GlideApp.with(context)
            .load(url)
            .error(R.drawable.ic_error_placeholder)
            .transform(FitCenter())
            .into(this)
}

