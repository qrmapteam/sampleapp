package app

import android.app.Application
import android.content.Context
import app.dagger.ApplicationComponent
import app.dagger.DaggerApplicationComponent
import app.dagger.modules.ApplicationModule
import app.ext.CrashReportingTree
import timber.log.Timber

class AndroidApp : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    val component: ApplicationComponent
        get() = applicationComponent

    override fun onCreate() {
        super.onCreate()
        initializeApplicationComponent()

        component.inject(this)
        this.initializeLogging()
    }

    private fun initializeApplicationComponent() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    private fun initializeLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    // Needed to replace the component with a test specific one
    fun setComponent(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }

    companion object {

        operator fun get(context: Context): AndroidApp {
            return context.applicationContext as AndroidApp
        }
    }
}
