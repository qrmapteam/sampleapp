package app.dagger

import android.content.Context
import app.AndroidApp
import app.dagger.modules.ApplicationModule
import com.fernandocejas.arrow.optional.Optional
import dagger.Component
import data.dagger.modules.DebugModule
import data.dagger.modules.NetworkModule
import data.dagger.scopes.AppScope
import data.sharedpreferences.SharedPrefsWrapper
import domain.executor.PostExecutionThread
import domain.executor.ThreadExecutor
import domain.repository.MovieRepository
import domain.transformer.DebugTransformer
import domain.transformer.SchedulerTransformer

@AppScope
@Component(modules = [ApplicationModule::class, NetworkModule::class, DebugModule::class])
interface ApplicationComponent {
    //Exposed to sub-graphs.
    fun context(): Context

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun gameRepository(): MovieRepository

    fun providesSchedulerTransformer(): SchedulerTransformer

    fun provideSharedPreferences(): SharedPrefsWrapper

    fun provideDebugTransformer(): Optional<DebugTransformer>

    fun inject(app: AndroidApp)
}
