package app.dagger.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import app.BuildConfig
import app.UIThread
import com.fernandocejas.arrow.optional.Optional
import dagger.Module
import dagger.Provides
import data.dagger.qualifiers.ApiUrl
import data.dagger.qualifiers.Debug
import data.dagger.qualifiers.Persistent
import data.dagger.scopes.AppScope
import data.executor.JobExecutor
import data.sharedpreferences.SharedPrefsWrapper
import data.transformer.DebugTimberTransformer
import domain.executor.PostExecutionThread
import domain.executor.ThreadExecutor
import domain.transformer.AndroidSchedulerTransformer
import domain.transformer.DebugTransformer
import domain.transformer.SchedulerTransformer
import timber.log.Timber
import java.net.URI
import java.net.URISyntaxException

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
class ApplicationModule(private val application: Application) {

    companion object {
        const val MOCK_MODE = false
    }

    @Provides
    @AppScope
    internal fun mockMode(): Boolean {
        return BuildConfig.DEBUG && MOCK_MODE
    }

    @Provides
    @AppScope
    @Debug
    internal fun debugMode(): Boolean {
        return BuildConfig.DEBUG
    }

    @Provides
    @AppScope
    internal fun provideApplicationContext(): Context {
        return this.application
    }

    @Provides
    @AppScope
    @ApiUrl
    internal fun apiUrl(sharedPrefsWrapper: SharedPrefsWrapper, @Debug debug: Boolean): String {
        var url = BuildConfig.BASE_URL
        if (debug) {
            val overrideUrl = sharedPrefsWrapper.overrideUrl
            if (overrideUrl.isPresent && overrideUrl.get().isNotBlank()) {
                url = overrideUrl.get()
            }
        }
        Timber.d("Using apiUrl: %s", url)

        return url
    }

    @Provides
    @AppScope
    internal fun versionCode(): Int {
        return BuildConfig.VERSION_CODE
    }

    @Provides
    @AppScope
    internal fun device(): String {
        return "${Build.MANUFACTURER} ${Build.MODEL}"
    }

    @Provides
    @AppScope
    internal fun operatingSystem(): String {
        return "Android ${Build.VERSION.RELEASE} (${Build.VERSION.SDK_INT})"
    }

    @Provides
    @AppScope
    internal fun provideDebugTransformer(@Debug isDebugging: Boolean): Optional<DebugTransformer> {
        return if (isDebugging) {
            Optional.fromNullable(DebugTimberTransformer())
        } else {
            Optional.absent()
        }
    }

    @Provides
    @AppScope
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @AppScope
    internal fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @AppScope
    internal fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("movietest", Context.MODE_PRIVATE)
    }

    @Provides
    @AppScope
    @Persistent
    internal fun providePersistentSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("movietestPersistent", Context.MODE_PRIVATE)
    }

    @Provides
    @AppScope
    internal fun providesSchedulerTransformer(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread): SchedulerTransformer {
        return AndroidSchedulerTransformer(threadExecutor, postExecutionThread)
    }
}
