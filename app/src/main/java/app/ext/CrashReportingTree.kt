package app.ext

import android.util.Log
import timber.log.Timber

/**
 * A tree which logs important information for crash reporting.
 */
class CrashReportingTree : Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.WARN || priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }

        super.log(priority, tag, message, t)
    }
}
