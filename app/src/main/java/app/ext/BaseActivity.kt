package app.ext

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity<out PRESENTER : Presenter> : AppCompatActivity() {

    private lateinit var disposables: CompositeDisposable

    abstract fun getPresenter(): PRESENTER

    abstract fun getView(): View?

    abstract fun injectDependencies()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        disposables = CompositeDisposable()

        injectDependencies()
        sendEvent(ActivityEvent.ON_CREATE)
        setContentView(getView())
        getPresenter().onCreate()
    }

    override fun onStart() {
        super.onStart()
        getPresenter().onStart()
        sendEvent(ActivityEvent.ON_START)
    }

    override fun onResume() {
        super.onResume()
        sendEvent(ActivityEvent.ON_RESUME)
        getPresenter().onResume()
    }

    override fun onPause() {
        sendEvent(ActivityEvent.ON_PAUSE)
        getPresenter().onPause()
        super.onPause()
    }

    override fun onStop() {
        getPresenter().onStop()
        sendEvent(ActivityEvent.ON_STOP)
        disposables.clear()
        super.onStop()
    }

    override fun setContentView(view: View?) {
        view?.let {
            super.setContentView(it)
        }
    }

    override fun onBackPressed() {
        if (!getPresenter().onBackPressed()) {
            super.onBackPressed()
        }
    }

    public override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        data?.let { getPresenter().onActivityResult(requestCode, resultCode, it) }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        getPresenter().onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun sendEvent(event: ActivityEvent) {
        event.withClass(javaClass)
    }
}
