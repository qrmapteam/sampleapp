package app.ext

import android.app.Activity
import android.content.Intent
import android.support.annotation.AnimRes
import app.R

fun Activity.startWithAnimation(intent: Intent,
                                @AnimRes animIn: Int = R.anim.activity_fade_in,
                                @AnimRes animOut: Int = R.anim.activity_fade_out) {
    this.startActivity(intent)
    this.overridePendingTransition(animIn, animOut)
}
