package app.ext

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
open class SampleAppGlideApp : AppGlideModule()
