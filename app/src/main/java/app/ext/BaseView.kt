package app.ext

import android.content.Context
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import app.R

open class BaseView(context: Context) : FrameLayout(context) {

    fun getSnackBar(@StringRes stringResId: Int): Snackbar {
        val snackbar = Snackbar.make(this, stringResId, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok) { }
        (snackbar.view.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView).maxLines = 10
        return snackbar
    }

    fun showSnackBar(message: String) {
        getSnackBar(message).show()
    }

    fun getSnackBar(message: String): Snackbar {
        val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok) { }
        (snackbar.view.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView).maxLines = 10
        return snackbar
    }
}