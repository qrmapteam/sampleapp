package app.ext

import android.content.Intent
import timber.log.Timber

abstract class Presenter {

    open fun onResume() {
        Timber.w("Presenter onResume called, but no implementation found.")
    }

    open fun onPause() {
        Timber.w("Presenter onPause called, but no implementation found.")
    }

    fun onStart() {
        Timber.w("Presenter onStart called, but no implementation found.")
    }

    fun onStop() {
        Timber.w("Presenter onStop called, but no implementation found.")
    }

    open fun onCreate() {
        Timber.w("Presenter onCreate called, but no implementation found.")
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        Timber.w("Presenter onRequestPermissionsResult called, but no implementation found.")
    }

    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        Timber.w("Presenter onActivityResult called, but no implementation found.")
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity as appropriate.  If not handled, calls through to the super method.
     *
     * @return if the event has been handled.
     */
    fun onBackPressed(): Boolean {
        return false
    }
}
