package app.ext

enum class ActivityEvent {
    ON_CREATE,
    ON_START,
    ON_STOP,
    ON_RESUME,
    ON_PAUSE,
    ON_DESTROY;

    lateinit var className: String

    fun withClass(clazz: Class<*>) {
        this.className = clazz.canonicalName
    }
}