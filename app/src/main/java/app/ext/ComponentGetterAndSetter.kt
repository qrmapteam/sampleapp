package app.ext

import data.dagger.injection.Injection

interface ComponentSetter<in COMPONENT : Injection<*>> {
    fun setComponent(component: COMPONENT)
}

interface ComponentGetter<out COMPONENT : Injection<*>> {
    fun getComponent(): COMPONENT
}
