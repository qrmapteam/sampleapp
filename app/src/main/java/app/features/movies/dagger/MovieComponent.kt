package app.features.movies.dagger

import app.features.movies.MovieFragment
import dagger.Subcomponent
import data.dagger.builder.SubcomponentBuilder
import data.dagger.injection.Injection

@MovieScope
@Subcomponent(modules = [(MovieModule::class)])
interface MovieComponent : Injection<MovieFragment> {
    @Subcomponent.Builder
    interface Builder : SubcomponentBuilder<MovieComponent> {
        fun movieModule(movieModule: MovieModule): MovieComponent.Builder
    }
}
