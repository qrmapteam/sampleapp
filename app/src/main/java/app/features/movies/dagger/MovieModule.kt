package app.features.movies.dagger

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides

@Module
class MovieModule(@get:Provides
                  @get:MovieScope
                  val fragment: Fragment)
