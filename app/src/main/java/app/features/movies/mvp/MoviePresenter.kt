package app.features.movies.mvp

import app.ext.Presenter
import app.features.movies.dagger.MovieScope
import domain.model.movie.MovieItemModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

@MovieScope
class MoviePresenter @Inject internal constructor(
        private val view: MovieView,
        private val model: MovieModel
) : Presenter() {

    private lateinit var disposables: CompositeDisposable

    override fun onCreate() {
        disposables = CompositeDisposable()
    }

    override fun onPause() {
        disposables.clear()
    }

    override fun onResume() {
        setupListeners()
    }

    private fun setupListeners() {
        disposables += observeMovies()
        disposables += observeMovieItemClicks()
    }

    private fun observeMovieItemClicks(): Disposable {
        return view.movieItemClickObserver
                .subscribeBy(
                        onNext = this::handleSelectedMovieItem,
                        onError = { Timber.e(it, "while observing movie item clicks") }
                )
    }

    private fun handleSelectedMovieItem(movieItemModel: MovieItemModel) {
        view.showSnackBar("Selected movie ${movieItemModel.title}")
    }

    private fun observeMovies(): Disposable {
        return model.movies
                .doOnSuccess { view.hideProgress() }
                .subscribeBy(
                        onSuccess = ::handleMoviesResponse,
                        onError = ::handleMoviesErrorResponse
                )
    }

    private fun handleMoviesResponse(movies: List<MovieItemModel>) {
        view.setMovies(movies)
    }

    private fun handleMoviesErrorResponse(throwable: Throwable?) {
        throwable ?: return
        Timber.e(throwable, "while observing movies")
    }

    companion object {
        const val TAG = "MoviePresenter"
    }
}
