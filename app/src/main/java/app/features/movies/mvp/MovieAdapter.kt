package app.features.movies.mvp

import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import app.R
import app.extensions.loadImage
import app.features.movies.dagger.MovieScope
import butterknife.BindView
import butterknife.ButterKnife
import com.jakewharton.rxbinding2.view.RxView
import domain.model.movie.MovieItemModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@MovieScope
class MovieAdapter @Inject internal constructor(
        val fragment: Fragment
) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    private val itemClickedSubject: PublishSubject<Int> = PublishSubject.create()
    private var movies: MutableList<MovieItemModel> = mutableListOf()

    val itemClickedObservable: Observable<MovieItemModel>
        get() = itemClickedSubject.map(movies::get)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_movie_item, parent, false)

        return ViewHolder(itemView).apply {
            RxView.clicks(itemView)
                    .takeUntil(RxView.detaches(parent))
                    .map { adapterPosition }
                    .subscribe(itemClickedSubject)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movieItemModel = movies[position]
        holder.setName(movieItemModel.title)
        holder.loadImage(IMAGE_API_URL + movieItemModel.backdropPath)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    fun clear() {
        movies.clear()
        notifyDataSetChanged()
    }

    internal fun setMovies(movies: List<MovieItemModel>) {
        this.movies = movies.toMutableList()
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.image)
        lateinit var image: ImageView

        @BindView(R.id.name)
        lateinit var name: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

        fun loadImage(url: String) {
            this.image.loadImage(url)
        }

        fun setName(name: String) {
            this.name.text = name
        }
    }

    companion object {
        const val IMAGE_API_URL = "https://image.tmdb.org/t/p/w500"
    }
}
