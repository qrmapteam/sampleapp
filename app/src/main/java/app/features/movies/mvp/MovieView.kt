package app.features.movies.mvp

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import androidx.core.view.isVisible
import app.R
import app.ext.BaseView
import app.features.movies.dagger.MovieScope
import butterknife.BindView
import butterknife.ButterKnife
import domain.model.movie.MovieItemModel
import io.reactivex.Observable
import javax.inject.Inject


@SuppressLint("ViewConstructor")
@MovieScope
class MovieView @Inject constructor(
        private val fragment: Fragment,
        private val movieAdapter: MovieAdapter
) : BaseView(fragment.context!!) {

    @BindView(R.id.loading_progress)
    internal lateinit var loadingProgress: ProgressBar

    @BindView(R.id.movie_list)
    internal lateinit var movieListView: RecyclerView

    internal val movieItemClickObserver: Observable<MovieItemModel>
        get() = movieAdapter.itemClickedObservable

    init {
        View.inflate(context, R.layout.fragment_movie, this)
        ButterKnife.bind(this)

        movieListView.adapter = movieAdapter
    }

    internal fun hideProgress() {
        loadingProgress.isVisible = false
    }

    fun setMovies(movies: List<MovieItemModel>) {
        movieAdapter.setMovies(movies)
    }
}
