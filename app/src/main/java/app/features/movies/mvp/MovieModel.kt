package app.features.movies.mvp

import app.extensions.random
import app.features.movies.dagger.MovieScope
import domain.interactors.movie.GetTopMoviesUseCase
import domain.model.movie.MovieItemModel
import domain.model.movie.MovieRequestModel
import io.reactivex.Single
import javax.inject.Inject

@MovieScope
class MovieModel @Inject internal constructor(
        private val getTopMoviesUseCase: GetTopMoviesUseCase
) {

    private val requestModel = MovieRequestModel(page = (0..3).random())

    val movies: Single<List<MovieItemModel>>
        get() = getTopMoviesUseCase.get(requestModel)
}
