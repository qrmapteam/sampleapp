package app.features.movies

import app.ext.BaseFragment
import app.ext.Presenter
import app.features.home.HomeActivity
import app.features.movies.dagger.MovieComponent
import app.features.movies.dagger.MovieModule
import app.features.movies.dagger.MovieScope
import app.features.movies.mvp.MoviePresenter
import app.features.movies.mvp.MovieView
import javax.inject.Inject

@MovieScope
class MovieFragment : BaseFragment<Presenter>() {

    @Inject
    internal lateinit var view: MovieView

    @Inject
    internal lateinit var presenter: MoviePresenter

    private val movieComponent: MovieComponent by lazy {
        (activity as HomeActivity).getComponent()
                .movieComponentBuilder()
                .movieModule(MovieModule(this))
                .build()
    }

    override fun getPresenter(): MoviePresenter {
        return presenter
    }

    override fun getView(): MovieView {
        return view
    }

    override fun injectDependencies() {
        movieComponent.inject(this)
    }

    companion object {
        fun newInstance(): MovieFragment {
            return MovieFragment()
        }
    }
}
