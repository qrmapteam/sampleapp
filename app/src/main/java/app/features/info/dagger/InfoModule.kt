package app.features.info.dagger

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides

@Module
class InfoModule(@get:Provides
                 @get:InfoScope
                 val fragment: Fragment)
