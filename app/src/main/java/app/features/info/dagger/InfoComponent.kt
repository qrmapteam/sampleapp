package app.features.info.dagger

import app.features.info.InfoFragment
import dagger.Subcomponent
import data.dagger.builder.SubcomponentBuilder
import data.dagger.injection.Injection

@InfoScope
@Subcomponent(modules = [(InfoModule::class)])
interface InfoComponent : Injection<InfoFragment> {
    @Subcomponent.Builder
    interface Builder : SubcomponentBuilder<InfoComponent> {
        fun infoModule(infoModule: InfoModule): InfoComponent.Builder
    }
}
