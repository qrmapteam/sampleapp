package app.features.info

import app.ext.BaseFragment
import app.ext.Presenter
import app.features.home.HomeActivity
import app.features.info.dagger.InfoComponent
import app.features.info.dagger.InfoModule
import app.features.info.dagger.InfoScope
import app.features.info.mvp.InfoPresenter
import app.features.info.mvp.InfoView
import javax.inject.Inject

@InfoScope
class InfoFragment : BaseFragment<Presenter>() {

    @Inject
    internal lateinit var view: InfoView

    @Inject
    internal lateinit var presenter: InfoPresenter

    private val movieComponent: InfoComponent by lazy {
        (activity as HomeActivity).getComponent()
                .infoComponentBuilder()
                .infoModule(InfoModule(this))
                .build()
    }

    override fun getPresenter(): InfoPresenter {
        return presenter
    }

    override fun getView(): InfoView {
        return view
    }

    override fun injectDependencies() {
        movieComponent.inject(this)
    }

    companion object {
        fun newInstance(): InfoFragment {
            return InfoFragment()
        }
    }
}
