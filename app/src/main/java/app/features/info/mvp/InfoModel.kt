package app.features.info.mvp

import app.features.info.dagger.InfoScope
import javax.inject.Inject

@InfoScope
class InfoModel @Inject internal constructor()
