package app.features.info.mvp

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.view.View
import android.widget.TextView
import app.BuildConfig
import app.R
import app.ext.BaseView
import app.features.info.dagger.InfoScope
import butterknife.BindView
import butterknife.ButterKnife
import javax.inject.Inject

@SuppressLint("ViewConstructor")
@InfoScope
class InfoView @Inject constructor(
        private val fragment: Fragment
) : BaseView(fragment.context!!) {

    @BindView(R.id.build_info)
    internal lateinit var buildInfo: TextView

    init {
        View.inflate(context, R.layout.fragment_info, this)
        ButterKnife.bind(this)

        buildInfo.text = context.getString(R.string.build_info, BuildConfig.VERSION_NAME)
    }
}
