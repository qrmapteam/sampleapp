package app.features.info.mvp

import app.ext.Presenter
import app.features.info.dagger.InfoScope
import app.features.movies.mvp.MovieModel
import app.features.movies.mvp.MovieView
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@InfoScope
class InfoPresenter @Inject internal constructor(
        private val view: InfoView,
        private val model: InfoModel
) : Presenter() {

    private lateinit var disposables: CompositeDisposable

    override fun onCreate() {
        disposables = CompositeDisposable()
    }

    private fun setupListeners() {

    }

    override fun onPause() {
        disposables.clear()
    }

    override fun onResume() {
        setupListeners()
    }


    companion object {
        const val TAG = "InfoPresenter"
    }
}
