package app.features.launch

import android.view.View
import app.AndroidApp
import app.ext.BaseActivity
import app.features.launch.dagger.DaggerLaunchComponent
import app.features.launch.dagger.LaunchModule
import app.features.launch.mvp.LaunchPresenter
import app.features.launch.mvp.LaunchView
import javax.inject.Inject

/* An activity which has the theme of Theme.NoDisplay - used to quickly determine which screen the
 * user should be directed to at launch
 */
class LaunchActivity : BaseActivity<LaunchPresenter>() {

    @Inject
    internal lateinit var view: LaunchView

    @Inject
    internal lateinit var presenter: LaunchPresenter

    override fun getPresenter(): LaunchPresenter {
        return presenter
    }

    override fun getView(): View? {
        return view
    }

    override fun injectDependencies() {
        DaggerLaunchComponent.builder()
                .applicationComponent(AndroidApp[this].component)
                .launchModule(LaunchModule(this))
                .build().inject(this)
    }
}
