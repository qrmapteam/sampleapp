package app.features.launch.dagger

import android.support.v7.app.AppCompatActivity

import dagger.Module
import dagger.Provides

@Module
class LaunchModule(@get:Provides
                   @get:LaunchScope
                   val activity: AppCompatActivity)
