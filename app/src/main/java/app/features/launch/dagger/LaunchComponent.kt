package app.features.launch.dagger

import app.dagger.ApplicationComponent
import app.features.launch.LaunchActivity

import dagger.Component

@LaunchScope
@Component(modules = [LaunchModule::class], dependencies = [ApplicationComponent::class])
interface LaunchComponent {
    fun inject(launchActivity: LaunchActivity)
}
