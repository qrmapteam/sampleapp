package app.features.launch.mvp

import app.ext.Presenter
import app.features.launch.dagger.LaunchScope
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@LaunchScope
class LaunchPresenter @Inject constructor(private val view: LaunchView,
                                          private val model: LaunchModel)
    : Presenter() {

    private lateinit var disposables: CompositeDisposable

    override fun onCreate() {
        disposables = CompositeDisposable()
    }

    override fun onResume() {
        disposables += observeTimeInterval()
    }

    override fun onPause() {
        disposables.clear()
    }

    private fun observeTimeInterval(): Disposable {
        return Observable.interval(TIME_OUT, TimeUnit.SECONDS).subscribe { _ -> view.startUnauthorisedSessionActivity() }
    }

    companion object {
        const val TIME_OUT = 2L
    }
}