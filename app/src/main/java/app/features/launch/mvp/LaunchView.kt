package app.features.launch.mvp

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.view.View
import app.R
import app.ext.BaseView
import app.ext.startWithAnimation
import app.features.home.HomeActivity
import app.features.launch.dagger.LaunchScope
import butterknife.ButterKnife
import javax.inject.Inject

@SuppressLint("ViewConstructor")
@LaunchScope
class LaunchView @Inject constructor(
        private val activity: AppCompatActivity
) : BaseView(activity) {

    init {
        View.inflate(context, R.layout.activity_launch, this)
        ButterKnife.bind(this)
    }

    fun startUnauthorisedSessionActivity() {
        activity.startWithAnimation(HomeActivity.getStartIntent(activity))
        finish()
    }

    private fun finish() {
        activity.finish()
    }
}
