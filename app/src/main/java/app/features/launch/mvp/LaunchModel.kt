package app.features.launch.mvp

import app.features.launch.dagger.LaunchScope
import javax.inject.Inject

@LaunchScope
class LaunchModel @Inject internal constructor()
