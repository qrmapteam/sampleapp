package app.features.home.dagger

import app.dagger.ApplicationComponent
import app.features.home.HomeActivity
import app.features.info.dagger.InfoComponent
import app.features.movies.dagger.MovieComponent
import dagger.Component
import data.dagger.injection.Injection

@HomeScope
@Component(modules = [(HomeModule::class)], dependencies = [(ApplicationComponent::class)])
interface HomeComponent : Injection<HomeActivity> {
    fun movieComponentBuilder(): MovieComponent.Builder
    fun infoComponentBuilder(): InfoComponent.Builder
}
