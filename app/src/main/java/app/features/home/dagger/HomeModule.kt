package app.features.home.dagger

import android.support.v7.app.AppCompatActivity
import app.features.info.dagger.InfoComponent
import app.features.movies.dagger.MovieComponent
import dagger.Module
import dagger.Provides

@Module(subcomponents = [
    MovieComponent::class,
    InfoComponent::class
])
class HomeModule(@get:Provides
                 @get:HomeScope
                 val activity: AppCompatActivity)
