package app.features.home.mvp

import app.ext.Presenter
import app.features.home.dagger.HomeScope
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

@HomeScope
class HomePresenter @Inject internal constructor(
        private val view: HomeView,
        private val model: HomeModel
) : Presenter() {

    private lateinit var disposables: CompositeDisposable

    override fun onCreate() {
        disposables = CompositeDisposable()
    }

    override fun onResume() {
        setupListeners()
        view.openSelectedItem()
    }

    private fun setupListeners() {
        disposables += observeNavigationViewClick()
    }

    override fun onPause() {
        disposables.clear()
    }

    private fun observeNavigationViewClick(): Disposable {
        return view.navigationViewClicksObserver
                .subscribeBy(
                        onNext = { view.openSelectedItem(it) },
                        onError = { Timber.e(it,"while observing navigation view clicks.") }
                )
    }

    companion object {
        const val TAG = "Home"
    }
}
