package app.features.home.mvp

import android.annotation.SuppressLint
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import app.R
import app.ext.BaseView
import app.features.home.dagger.HomeScope
import app.features.info.InfoFragment
import app.features.movies.MovieFragment
import butterknife.BindView
import butterknife.ButterKnife
import com.jakewharton.rxbinding2.support.design.widget.RxNavigationView
import io.reactivex.Observable
import javax.inject.Inject

@SuppressLint("ViewConstructor")
@HomeScope
class HomeView @Inject constructor(
        private val activity: AppCompatActivity
) : BaseView(activity) {

    @BindView(R.id.toolbar)
    internal lateinit var toolbar: Toolbar

    @BindView(R.id.drawer_layout)
    internal lateinit var drawerLayout: DrawerLayout

    @BindView(R.id.navigation_view)
    internal lateinit var navigationView: NavigationView

    val navigationViewClicksObserver: Observable<MenuItem>
        get() = RxNavigationView.itemSelections(navigationView)

    init {
        View.inflate(context, R.layout.activity_home, this)
        ButterKnife.bind(this)

        setActionBar()
    }

    private fun setActionBar() {
        activity.setSupportActionBar(toolbar)
        val actionbar: ActionBar? = activity.supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu)
        }
    }

    fun openNavigationDrawer() {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    fun openSelectedItem(menuItem: MenuItem = navigationView.menu.findItem(R.id.nav_movies)) {
        menuItem.isChecked = true
        drawerLayout.closeDrawers()
        when (menuItem.itemId) {
            R.id.nav_movies -> {
                toolbar.title = activity.getString(R.string.movie_screen)
                navigateToMovies()
            }
            R.id.nav_info -> {
                toolbar.title = activity.getString(R.string.info_screen)
                navigateToInfo()
            }
        }
    }

    private fun navigateToMovies() {
        loadTopLevelFragment(MovieFragment.newInstance(), MovieFragment::class.java.name)
    }

    private fun navigateToInfo() {
        loadTopLevelFragment(InfoFragment.newInstance(), InfoFragment::class.java.name)
    }

    private fun loadTopLevelFragment(fragment: Fragment, tag: String) {
        val fragmentManager = activity.supportFragmentManager

        fragmentManager.findFragmentByTag(tag)?.let {
            return
        }

        // Clear off all other fragments when we select a top level navigation
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout, fragment, tag)
        fragmentTransaction.commit()
    }
}
