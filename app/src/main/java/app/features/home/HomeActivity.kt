package app.features.home

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import android.view.View
import app.AndroidApp
import app.ext.BaseActivity
import app.ext.ComponentGetter
import app.ext.ComponentSetter
import app.features.home.dagger.DaggerHomeComponent
import app.features.home.dagger.HomeComponent
import app.features.home.dagger.HomeModule
import app.features.home.dagger.HomeScope
import app.features.home.mvp.HomePresenter
import app.features.home.mvp.HomeView
import javax.inject.Inject

@HomeScope
class HomeActivity : BaseActivity<HomePresenter>(),
        ComponentSetter<HomeComponent>,
        ComponentGetter<HomeComponent> {

    @Inject
    lateinit var view: HomeView

    @Inject
    lateinit var homePresenter: HomePresenter

    private var homeComponent: HomeComponent? = null

    override fun getPresenter(): HomePresenter {
        return homePresenter
    }

    override fun getView(): View {
        return view
    }

    override fun getComponent(): HomeComponent {
        homeComponent?.let {
            return it
        }

        val component = DaggerHomeComponent.builder()
                .applicationComponent(AndroidApp[this].component)
                .homeModule(HomeModule(this))
                .build()

        homeComponent = component
        return component
    }

    override fun setComponent(component: HomeComponent) {
        homeComponent = component
    }

    override fun injectDependencies() {
        getComponent().inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                view.openNavigationDrawer()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun getStartIntent(context: Context): Intent {
            val intent = Intent(context, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            return intent
        }
    }
}
