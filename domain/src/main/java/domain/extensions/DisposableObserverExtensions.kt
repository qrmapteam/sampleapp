package domain.extensions

import io.reactivex.CompletableSource
import io.reactivex.MaybeSource
import io.reactivex.ObservableSource
import io.reactivex.SingleSource
import io.reactivex.exceptions.OnErrorNotImplementedException
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.plugins.RxJavaPlugins

private val onNextStub: (Any) -> Unit = {}
private val onErrorStub: (Throwable) -> Unit = { RxJavaPlugins.onError(OnErrorNotImplementedException(it)) }
private val onCompleteStub = {}

fun <T : Any> MaybeSource<T>.subscribeAndDispose(onSuccess: (T) -> Unit = onNextStub,
                                                 onError: (Throwable) -> Unit = onErrorStub,
                                                 onComplete: () -> Unit = onCompleteStub) {
    val observer = object : DisposableMaybeObserver<T>() {
        override fun onSuccess(t: T) {
            onSuccess(t)
            dispose()
        }

        override fun onComplete() {
            onComplete()
            dispose()
        }

        override fun onError(e: Throwable) {
            onError(e)
            dispose()
        }
    }
    subscribe(observer)
}

fun <T : Any> SingleSource<T>.subscribeAndDispose(onSuccess: (T) -> Unit = onNextStub,
                                                  onError: (Throwable) -> Unit = onErrorStub) {
    val observer = object : DisposableSingleObserver<T>() {
        override fun onSuccess(t: T) {
            onSuccess(t)
            dispose()
        }

        override fun onError(e: Throwable) {
            onError(e)
            dispose()
        }
    }
    subscribe(observer)
}

fun <T : Any> ObservableSource<T>.subscribeAndDispose(onNext: (T) -> Unit = onNextStub,
                                                      onError: (Throwable) -> Unit = onErrorStub,
                                                      onComplete: () -> Unit = onCompleteStub) {
    val observer = object : DisposableObserver<T>() {
        override fun onNext(t: T) {
            onNext(t)
            dispose()
        }

        override fun onComplete() {
            onComplete()
            dispose()
        }

        override fun onError(e: Throwable) {
            onError(e)
            dispose()
        }
    }
    subscribe(observer)
}

fun CompletableSource.subscribeAndDispose(onComplete: () -> Unit = onCompleteStub,
                                          onError: (Throwable) -> Unit = onErrorStub) {
    val observer = object : DisposableCompletableObserver() {
        override fun onComplete() {
            onComplete()
            dispose()
        }

        override fun onError(e: Throwable) {
            onError(e)
            dispose()
        }
    }
    subscribe(observer)
}
