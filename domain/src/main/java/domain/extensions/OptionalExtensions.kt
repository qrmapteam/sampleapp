package domain.extensions

import com.fernandocejas.arrow.optional.Optional

val <T> T?.asOptional: Optional<T>
    get() = this?.let {
        Optional.fromNullable(it)
    } ?: Optional.absent()

val <T> Optional<T>.fromOptional: T? get () = if (this.isPresent) this.get() as T else null

inline fun <T, R> Optional<T>.fromOptional(block: (T) -> R?): R? {
    return if (this.isPresent) {
        block(get())
    } else {
        null
    }
}

fun Optional<String>.toStringOrEmpty(): String {
    if (isPresent) {
        return get()
    }
    return ""
}

fun Optional<Long>.toRemainingTime(timeoutLength: Long): Long {
    return when {
        this.isPresent -> {
            var timeRemaining = timeoutLength - (System.currentTimeMillis() - this.get())
            if (timeRemaining < 0) {
                timeRemaining = 0L
            }
            timeRemaining
        }
        else -> 0L
    }
}