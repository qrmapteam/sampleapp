package domain.model.movie

data class MovieItemModel(val id: Int,
                          val title: String,
                          val backdropPath: String)
