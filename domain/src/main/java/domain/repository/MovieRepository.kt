package domain.repository

import domain.model.movie.MovieItemModel
import io.reactivex.Single

interface MovieRepository {

    fun getTopRatedMovies(token: String, language: String, page: Int): Single<List<MovieItemModel>>
}
