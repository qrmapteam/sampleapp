package domain.repository

import com.fernandocejas.arrow.optional.Optional

interface DebugRepository {

    val overrideServerUrl: Optional<String>

    fun setOverrideServerUrl(url: String)
}