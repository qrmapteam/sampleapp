package domain.transformer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import domain.executor.PostExecutionThread;
import domain.executor.ThreadExecutor;
import io.reactivex.Completable;
import io.reactivex.CompletableTransformer;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.schedulers.Schedulers;

public class AndroidSchedulerTransformer implements SchedulerTransformer {

  private final Scheduler subscribeScheduler;
  private final Scheduler observeScheduler;

  @Inject
  public AndroidSchedulerTransformer(final ThreadExecutor threadExecutor, final PostExecutionThread postExecutionThread) {
    subscribeScheduler = Schedulers.from(threadExecutor);
    observeScheduler = postExecutionThread.getScheduler();
  }

  @Override
  public <T> ObservableTransformer<T, T> applyObservableSchedulers() {
    return observable -> observable.subscribeOn(subscribeScheduler)
                                   .observeOn(observeScheduler);
  }

  @Override
  public CompletableTransformer applyCompletableSchedulers() {
    return completable -> completable.subscribeOn(subscribeScheduler)
                                     .observeOn(observeScheduler);
  }

  @Override
  public <T> SingleTransformer<T, T> applySingleSchedulers() {
    return single -> single.subscribeOn(subscribeScheduler)
                           .observeOn(observeScheduler);
  }

  @NotNull
  @Override
  public Completable applySubscribeScheduler(@NotNull final Completable completable) {
    return completable.subscribeOn(subscribeScheduler);
  }

  @NotNull
  @Override
  public <T> Single<T> applySubscribeScheduler(@NotNull final Single<T> single) {
    return single.subscribeOn(subscribeScheduler);
  }

  @NotNull
  @Override
  public <T> Observable<T> applySubscribeScheduler(@NotNull final Observable<T> observable) {
    return observable.subscribeOn(subscribeScheduler);
  }
}
