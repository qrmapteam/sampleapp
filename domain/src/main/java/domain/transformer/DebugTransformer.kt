package domain.transformer

import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer

interface DebugTransformer {
    fun <T> applyObservableDebugger(tag: String): ObservableTransformer<T, T>

    fun applyCompletableDebugger(tag: String): CompletableTransformer

    fun <T> applySingleDebugger(tag: String): SingleTransformer<T, T>
}