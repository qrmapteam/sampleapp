package domain.interactors

import com.google.auto.value.AutoValue
import io.reactivex.Single

abstract class SingleParameterisedUseCase<T, P> : BaseUseCase() {

    protected abstract fun build(params: P): Single<T>

    private fun make(params: Params<P>?, wrap: Boolean): Single<T> {
        if (params == null) {
            throw IllegalArgumentException("Params must be defined")
        }

        val single = wrapDebug(build(params.param()))
        return if (wrap) wrap(single) else single
    }

    operator fun get(params: P): Single<T> {
        return make(Params.create(params), true)
    }

    fun chain(params: P): Single<T> {
        return make(Params.create(params), false)
    }

    private fun wrap(observable: Single<T>): Single<T> {
        return observable.compose(schedulerTransformer()!!.applySingleSchedulers())
    }

    private fun wrapDebug(observable: Single<T>): Single<T> {
        return if (debugTransformer()!!.isPresent && isDebugLogsEnabled) {
            observable.compose(debugTransformer()!!.get().applySingleDebugger(javaClass.simpleName))
        } else observable
    }

    @AutoValue
    abstract class Params<P> {

        abstract fun param(): P

        @AutoValue.Builder
        abstract class Builder<P> {
            abstract fun param(param: P): Builder<P>

            abstract fun build(): Params<P>
        }

        companion object {

            fun <P> create(param: P): Params<P> {
                return Params.builder<P>().param(param)
                        .build()
            }

            fun <P> builder(): Builder<P> {
                return AutoValue_SingleParameterisedUseCase_Params.Builder()
            }
        }
    }
}
