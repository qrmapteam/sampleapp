package domain.interactors

import com.fernandocejas.arrow.optional.Optional
import domain.transformer.DebugTransformer
import domain.transformer.SchedulerTransformer
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

abstract class BaseUseCase {

    private lateinit var schedulerTransformer: SchedulerTransformer
    private lateinit var debugTransformer: Optional<DebugTransformer>

    protected val isDebugLogsEnabled: Boolean
        get() = true

    @Inject
    fun setSchedulerTransformer(schedulerTransformer: SchedulerTransformer) {
        this.schedulerTransformer = schedulerTransformer
    }

    @Inject
    fun setDebugTransformer(debugTransformer: Optional<DebugTransformer>) {
        this.debugTransformer = debugTransformer
    }

    protected fun schedulerTransformer(): SchedulerTransformer? {
        return schedulerTransformer
    }

    protected fun debugTransformer(): Optional<DebugTransformer>? {
        return debugTransformer
    }

    fun Completable.applySubscribeScheduler(): Completable {
        return schedulerTransformer.applySubscribeScheduler(this)
    }

    fun <T> Observable<T>.applySubscribeScheduler(): Observable<T> {
        return schedulerTransformer.applySubscribeScheduler(this)
    }

    fun <T> Single<T>.applySubscribeScheduler(): Single<T> {
        return schedulerTransformer.applySubscribeScheduler(this)
    }
}
