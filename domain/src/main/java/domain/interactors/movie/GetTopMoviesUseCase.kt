package domain.interactors.movie

import domain.interactors.SingleParameterisedUseCase
import domain.model.movie.MovieItemModel
import domain.model.movie.MovieRequestModel
import domain.repository.MovieRepository
import io.reactivex.Single
import javax.inject.Inject

class GetTopMoviesUseCase @Inject constructor(private val movieRepository: MovieRepository)
    : SingleParameterisedUseCase<List<MovieItemModel>, MovieRequestModel>() {

    // TODO: Add here auth token (api_key) and language part - getting from shared preferences

    override fun build(requestModel: MovieRequestModel): Single<List<MovieItemModel>> {
        return movieRepository.getTopRatedMovies("7b131eca7218b1f8d2a1a968d4140c7c", "en-US", requestModel.page)
    }
}
