package data.transformer;

import domain.transformer.DebugTransformer;
import io.reactivex.CompletableTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;
import timber.log.Timber;

public class DebugTimberTransformer implements DebugTransformer {

  @Override
  public <T> ObservableTransformer<T, T> applyObservableDebugger(final String tag) {
    return observable -> observable.doOnNext(data -> Timber.v("[%s] [onNext] [Thread: %s] [observable] [response: %s]",
                                                              tag,
                                                              Thread.currentThread().getName(),
                                                              data.getClass()))
                                   .doOnSubscribe(disposable -> Timber.v("[%s] [subscribe] [Thread:%s] [observable]",
                                                                         tag,
                                                                         Thread.currentThread().getName()))
                                   .doOnDispose(() -> Timber.v("[%s] [dispose] [observable]", tag));
  }

  @Override
  public CompletableTransformer applyCompletableDebugger(final String tag) {
    return completable -> completable.doOnComplete(() -> Timber.v("[%s] [onComplete] [Thread:%s] [completable]",
                                                                  tag,
                                                                  Thread.currentThread().getName()))
                                     .doOnSubscribe(disposable -> Timber.v("[%s] [subscribe] [Thread:%s] [completable]",
                                                                           tag,
                                                                           Thread.currentThread().getName()))
                                     .doOnDispose(() -> Timber.v("[%s] [dispose] [completable]", tag));
  }

  @Override
  public <T> SingleTransformer<T, T> applySingleDebugger(final String tag) {
    return single -> single.doOnSuccess(data -> Timber.d("[%s] [onSuccess] [Thread:%s] [single] [response: %s] ",
                                                         tag,
                                                         Thread.currentThread().getName(),
                                                         data.getClass()))
                           .doOnSubscribe(disposable -> Timber.v("[%s] [subscribe] [Thread:%s] [single]",
                                                                 tag,
                                                                 Thread.currentThread().getName()))
                           .doOnDispose(() -> Timber.v("[%s] [dispose] [single]", tag));
  }
}
