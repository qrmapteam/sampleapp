package data.ext

object Constants {
    // This is to ensure app upgrade compatibility for Shared Preferences.
    const val LEGACY_PREFERENCES_PREFIX = "data.i.a"
}