package data.sharedpreferences

import android.content.SharedPreferences
import com.fernandocejas.arrow.optional.Optional
import data.dagger.qualifiers.Persistent
import data.dagger.scopes.AppScope
import data.ext.Constants
import data.extensions.clear
import data.extensions.set
import javax.inject.Inject

@AppScope
class SharedPrefsWrapper @Inject constructor(private val sharedPreferences: SharedPreferences,
                                             @Persistent private val persistentSharedPreferences: SharedPreferences) {

    var bestScore: Int
        get() {
            return this.sharedPreferences.getInt(GAME_BEST_SCORE, 0)
        }
        set(value) {
            this.sharedPreferences[GAME_BEST_SCORE] = value
        }

    var overrideUrl: Optional<String>
        get() {
            return Optional.fromNullable(this.persistentSharedPreferences.getString(OVERRIDE_URL, ""))
        }
        set(value) {
            this.persistentSharedPreferences[OVERRIDE_URL] = value.get()
        }

    fun clear() {
        sharedPreferences.clear()
    }

    companion object {
        private const val GAME_BEST_SCORE = "${Constants.LEGACY_PREFERENCES_PREFIX}GAME_BEST_SCORE_FLAG"
        private const val OVERRIDE_URL = Constants.LEGACY_PREFERENCES_PREFIX + "OVERRIDE_URL"
    }
}
