package data.repository

import data.entities.movie.MoviesResponseEntity
import data.net.RestApi
import data.sharedpreferences.SharedPrefsWrapper
import domain.model.movie.MovieItemModel
import domain.repository.MovieRepository
import io.reactivex.Single
import javax.inject.Inject

class MovieDataRepository @Inject constructor(
        private val gameRestApi: RestApi.Movie,
        private val sharedPrefsWrapper: SharedPrefsWrapper
) : MovieRepository {

    override fun getTopRatedMovies(token: String, language: String, page: Int): Single<List<MovieItemModel>> {
        return gameRestApi.topRatedMovies(token, language, page)
                .map<List<MovieItemModel>> { moviesResponse -> MoviesResponseEntity.Mapper().transform(moviesResponse) }
    }
}
