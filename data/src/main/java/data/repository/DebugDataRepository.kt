package data.repository

import data.sharedpreferences.SharedPrefsWrapper
import domain.repository.DebugRepository

import com.fernandocejas.arrow.optional.Optional

import javax.inject.Inject

class DebugDataRepository @Inject constructor(internal var sharedPrefsWrapper: SharedPrefsWrapper)
    : DebugRepository {

    override val overrideServerUrl: Optional<String>
        get() = sharedPrefsWrapper.overrideUrl

    override fun setOverrideServerUrl(url: String) {
        sharedPrefsWrapper.overrideUrl = Optional.of(url)
    }
}