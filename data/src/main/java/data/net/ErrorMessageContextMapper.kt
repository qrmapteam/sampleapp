package data.net

interface ErrorMessageContextMapper {
    fun getContextAwareMessageResourceId(responseCode: Int, messageContext: MessageContext): Int
}
