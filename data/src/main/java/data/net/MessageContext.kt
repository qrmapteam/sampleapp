package data.net

enum class MessageContext {
    LOGIN,
    REGISTRATION
}