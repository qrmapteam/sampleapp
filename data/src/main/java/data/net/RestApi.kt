package data.net

import data.entities.movie.MoviesResponseEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

/**
 * RestApi for retrieving data from the network.
 */
class RestApi {

    interface Movie {
        @Headers("Accept: application/json", "Connection: close")
        @GET("/3/movie/top_rated")
        fun topRatedMovies(@Query("api_key") apiKey: String,
                           @Query("language") language: String,
                           @Query("page") page: Int): Single<MoviesResponseEntity>
    }
}
