package data.net;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.HttpURLConnection;

import javax.annotation.Nullable;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

public class RetrofitException extends RuntimeException {
  @Nullable
  private final String url;

  @Nullable
  private final Response response;
  private final Kind kind;
  private final String exceptionBodyDescription;

  @Nullable
  private final Retrofit retrofit;

  private final ErrorMessageContextMapper messageContextMapper;

  @Nullable
  private final String errorBodyContent;

  RetrofitException(final String message, final String url, final Response response, final Kind kind, final Throwable exception,
                    final Retrofit retrofit, final ErrorMessageContextMapper messageContextMapper) {
    super(message, exception);
    this.url = url;
    this.response = response;
    this.errorBodyContent = getErrorBodyAsString();
    this.kind = kind;
    this.retrofit = retrofit;
    this.messageContextMapper = messageContextMapper;
    this.exceptionBodyDescription = new RetrofitExceptionBodyDescription(errorBodyContent).getDescription();
  }

  /**
   * Getting error body here and caching it because {@code response.errorBody.string()}
   * should be called once. If called second time it will return empty string.
   *
   * @return Error body content as String
   */
  @Nullable
  private String getErrorBodyAsString() {
    if (response != null && response.errorBody() != null) {
      try {
        return response.errorBody().string();
      } catch (final IOException e) {
        Timber.e(e);
        return null;
      }
    } else {
      return null;
    }
  }

  @Nullable
  public String getErrorBodyContent() {
    return errorBodyContent;
  }

  public static RetrofitException httpError(final String url, final Response response, final Retrofit retrofit,
                                            final ErrorMessageContextMapper messageContextMapper) {
    final String message = response.code() + " " + response.message();
    return new RetrofitExceptionBuilder().setMessage(message)
                                         .setUrl(url)
                                         .setResponse(response)
                                         .setKind(Kind.HTTP)
                                         .setRetrofit(retrofit)
                                         .setErrorMessageContextMapper(messageContextMapper)
                                         .createRetrofitException();
  }

  public static RetrofitException networkError(final IOException exception) {
    return new RetrofitExceptionBuilder().setMessage(exception.getMessage())
                                         .setKind(Kind.NETWORK)
                                         .setException(exception)
                                         .createRetrofitException();
  }

  public static RetrofitException unexpectedError(final Throwable exception) {
    return new RetrofitExceptionBuilder().setMessage(exception.getMessage())
                                         .setKind(Kind.UNEXPECTED)
                                         .setException(exception)
                                         .createRetrofitException();
  }

  public static boolean isHttpError(final Throwable throwable) {
    return throwable instanceof RetrofitException && ((RetrofitException) throwable).getKind() == Kind.HTTP;
  }

  public static boolean isNotFoundError(final Throwable throwable) {
    return isHttpError(throwable) && ((RetrofitException) throwable).response.code() == HttpURLConnection.HTTP_NOT_FOUND;
  }

  public static boolean isBadRequestError(final Throwable throwable) {
    return isHttpError(throwable) && ((RetrofitException) throwable).response.code() == HttpURLConnection.HTTP_BAD_REQUEST;
  }

  @Nullable
  public String getErrorBodyDescription() {
    return exceptionBodyDescription;
  }

  /**
   * The request URL which produced the error.
   */
  public String getUrl() {
    return url;
  }

  /**
   * Response object containing status code, headers, body, etc.
   */
  public Response getResponse() {
    return response;
  }

  /**
   * The event kind which triggered this error.
   */
  public Kind getKind() {
    return kind;
  }

  public int getHttpErrorMessageResId(final MessageContext messageContext) {
    if (kind != Kind.HTTP) {
      Timber.w("Error message only available for http exception types");
      return -1;
    }

    return messageContextMapper.getContextAwareMessageResourceId(response.code(), messageContext);
  }

  public int getHttpErrorMessageResId() {
    return getHttpErrorMessageResId(null);
  }

  /**
   * The Retrofit this request was executed on
   */
  public Retrofit getRetrofit() {
    return retrofit;
  }

  /**
   * HTTP response body converted to specified {@code type}. {@code null} if there is no
   * response.
   *
   * @throws IOException if unable to convert the body to the specified {@code type}.
   */
  public <T> T getErrorBodyAs(final Class<T> type) throws IOException {
    if (response == null || response.errorBody() == null) {
      return null;
    }
    final Converter<ResponseBody, T> converter = retrofit.responseBodyConverter(type, new Annotation[0]);
    return converter.convert(response.errorBody());
  }

  /**
   * Identifies the event kind which triggered a {@link RetrofitException}.
   */
  public enum Kind {
    /**
     * An {@link IOException} occurred while communicating to the server.
     */
    NETWORK,
    /**
     * A non-200 HTTP status code was received from the server.
     */
    HTTP,
    /**
     * An internal error occurred while attempting to execute a request. It is best practice to
     * re-throw this exception so your application crashes.
     */
    UNEXPECTED
  }

  public static class RetrofitExceptionBuilder {
    private ErrorMessageContextMapper messageContextMapper;
    private String message;
    private String url;
    private Response response;
    private Kind kind;
    private Throwable exception;
    private Retrofit retrofit;

    public RetrofitExceptionBuilder setErrorMessageContextMapper(final ErrorMessageContextMapper messageContextMapper) {
      this.messageContextMapper = messageContextMapper;
      return this;
    }

    public RetrofitExceptionBuilder setMessage(final String message) {
      this.message = message;
      return this;
    }

    public RetrofitExceptionBuilder setUrl(final String url) {
      this.url = url;
      return this;
    }

    public RetrofitExceptionBuilder setResponse(final Response response) {
      this.response = response;
      return this;
    }

    public RetrofitExceptionBuilder setKind(final Kind kind) {
      this.kind = kind;
      return this;
    }

    public RetrofitExceptionBuilder setException(final Throwable exception) {
      this.exception = exception;
      return this;
    }

    public RetrofitExceptionBuilder setRetrofit(final Retrofit retrofit) {
      this.retrofit = retrofit;
      return this;
    }

    public RetrofitException createRetrofitException() {
      return new RetrofitException(message, url, response, kind, exception, retrofit, messageContextMapper);
    }
  }
}
