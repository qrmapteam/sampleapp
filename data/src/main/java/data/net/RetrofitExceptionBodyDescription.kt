package data.net

import org.json.JSONObject

class RetrofitExceptionBodyDescription @JvmOverloads constructor(errorBodyAsString: String?,
                                                                 private val descriptionTag: String = "description") {

    var description: String? = null
        private set

    init {
        errorBodyAsString?.let {
            description = try {
                JSONObject(it).getString(descriptionTag)
            } catch (exception: Exception) {
                null
            }
        }
    }
}
