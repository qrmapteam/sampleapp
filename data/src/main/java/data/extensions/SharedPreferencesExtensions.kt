package data.extensions

import android.content.SharedPreferences
import com.squareup.moshi.Moshi

inline operator fun <reified T> SharedPreferences.get(key: String): T? {
    val adapter = Moshi.Builder().build().adapter(T::class.java)
    val json: String? = getString(key, null)

    return try {
        json?.let {
            adapter.fromJson(it)
        }
    } catch (ex: Throwable) {
        null
    }
}

inline operator fun <reified T> SharedPreferences.set(key: String, value: T) {
    value?.let {
        when (value) {
            is String -> edit().putString(key, value).apply()
            is Boolean -> edit().putBoolean(key, value).apply()
            is Float -> edit().putFloat(key, value).apply()
            is Int -> edit().putInt(key, value).apply()
            is Long -> edit().putLong(key, value).apply()
            else -> {
                val adapter = Moshi.Builder().build().adapter(T::class.java)
                edit().putString(key, adapter.toJson(value)).apply()
            }
        }
    }
}

fun SharedPreferences.remove(key: String) {
    edit().remove(key).apply()
}

fun SharedPreferences.clear() {
    edit().clear().apply()
}
