package data.dagger.modules

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import data.dagger.qualifiers.ApiUrl
import data.dagger.qualifiers.MovieRetrofit
import data.dagger.scopes.AppScope
import data.net.MoshiJsonAdapterFactory
import data.net.RestApi
import data.net.RestApiBuilder
import data.repository.MovieDataRepository
import domain.repository.MovieRepository
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class NetworkModule {

    @AppScope
    @Provides
    fun provideMoshi(): Moshi {
        val factory = MoshiJsonAdapterFactory.create()
        val nullSafeMoshiJsonAdapterFactory =
                JsonAdapter.Factory { type, annotations, moshi ->
                    val jsonAdapter = factory.create(type, annotations, moshi)
                    jsonAdapter?.nullSafe() ?: run { null }
                }
        return Moshi.Builder().add(nullSafeMoshiJsonAdapterFactory).build()
    }

    @AppScope
    @Provides
    @MovieRetrofit
    internal fun provideMovieRetrofit(@ApiUrl apiUrl: String, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }

    @AppScope
    @Provides
    internal fun provideMovieEndpoints(@MovieRetrofit retrofit: Retrofit): RestApi.Movie {
        return RestApiBuilder(retrofit).build(RestApi.Movie::class.java)
    }

    @AppScope
    @Provides
    internal fun provideMovieRepository(movieDataRepository: MovieDataRepository): MovieRepository {
        return movieDataRepository
    }
}
