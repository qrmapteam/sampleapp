package data.dagger.modules

import dagger.Module
import dagger.Provides
import data.dagger.scopes.AppScope
import data.repository.DebugDataRepository
import domain.repository.DebugRepository

@Module
class DebugModule {

    @Provides
    @AppScope
    internal fun provideDebugRepository(debugDataRepository: DebugDataRepository): DebugRepository {
        return debugDataRepository
    }
}
