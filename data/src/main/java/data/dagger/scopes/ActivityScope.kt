package data.dagger.scopes

import javax.inject.Scope

@Scope
annotation class ActivityScope
