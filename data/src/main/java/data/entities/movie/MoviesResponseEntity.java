package data.entities.movie;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.util.ArrayList;
import java.util.List;

import data.entities.MapperContract;
import domain.model.movie.MovieItemModel;

@AutoValue
public abstract class MoviesResponseEntity {
    public static MoviesResponseEntity create(final List<MovieEntity> movies) {
        return new AutoValue_MoviesResponseEntity(movies);
    }

    public static JsonAdapter<MoviesResponseEntity> jsonAdapter(final Moshi moshi) {
        return new AutoValue_MoviesResponseEntity.MoshiJsonAdapter(moshi);
    }

    @Json(name = "results")
    public abstract List<MovieEntity> results();

    public static class Mapper implements MapperContract<MoviesResponseEntity, List<MovieItemModel>> {

        @Override
        public List<MovieItemModel> transform(final MoviesResponseEntity from) {
            final List<MovieItemModel> movies = new ArrayList<>();
            final MovieEntity.Mapper mapper = new MovieEntity.Mapper();
            for (final MovieEntity policy : from.results()) {
                movies.add(mapper.transform(policy));
            }
            return movies;
        }
    }
}
