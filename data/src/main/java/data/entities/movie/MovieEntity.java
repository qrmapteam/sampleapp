package data.entities.movie;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import data.entities.MapperContract;
import domain.model.movie.MovieItemModel;

@AutoValue
public abstract class MovieEntity {

    public static JsonAdapter<MovieEntity> jsonAdapter(final Moshi moshi) {
        return new AutoValue_MovieEntity.MoshiJsonAdapter(moshi);
    }

    public static MovieEntity create(final int id, final String title, final String backdropPath) {
        return new AutoValue_MovieEntity(id, title, backdropPath);
    }

    @Json(name = "id")
    public abstract int id();

    @Json(name = "title")
    public abstract String title();

    @Json(name = "backdrop_path")
    public abstract String backdropPath();

    public static class Mapper implements MapperContract<MovieEntity, MovieItemModel> {

        @Override
        public MovieItemModel transform(final MovieEntity from) {
            return new MovieItemModel(from.id(), from.title(), from.backdropPath());
        }
    }
}
