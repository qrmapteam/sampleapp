package data.entities

interface MapperContractParam<F, T, P> {
    @Throws(IllegalStateException::class)
    fun transform(from: F, param: P): T
}

