package data.entities

interface MapperContract<F, T> {
    fun transform(from: F): T
}
